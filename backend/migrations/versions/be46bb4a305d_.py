"""empty message

Revision ID: be46bb4a305d
Revises: 5d3059e7be0f
Create Date: 2021-05-21 14:06:50.695687

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'be46bb4a305d'
down_revision = '5d3059e7be0f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('task_emp', sa.Column('isdeleted', sa.Boolean(), nullable=True))
    op.add_column('task_emp', sa.Column('updatedAt', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('task_emp', 'updatedAt')
    op.drop_column('task_emp', 'isdeleted')
    # ### end Alembic commands ###
