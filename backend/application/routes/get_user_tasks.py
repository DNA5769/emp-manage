# pylint: disable=import-error
from application.models.User import User
from application.utilities.required_params import required_params
from application.utilities.required_token import required_token
from flask import Blueprint, request, jsonify

get_user_tasks = Blueprint('get_user_tasks', __name__)

@get_user_tasks.route('/', methods=['GET'])
@required_token()
def get_user_tasks_handler(pk, user):
  try:
    user = User.get(pk)

    if user is None:
      return jsonify({ 'message': f"Task with id {pk} not found" }), 404
    
    return jsonify({ "tasks": user.get_tasks() }), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 500
