# pylint: disable=import-error
from application.models.Task import Task
from application.utilities.required_token import required_token
from application.utilities.required_admin import required_admin
from flask import Blueprint, jsonify

get_tasks = Blueprint('get_tasks', __name__)

@get_tasks.route('/')
@required_token()
@required_admin()
def home(user):
  return jsonify(Task.get_all()), 200