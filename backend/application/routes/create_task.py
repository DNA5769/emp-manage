# pylint: disable=import-error
from application.models.Task import Task
from application.utilities.required_params import required_params
from application.utilities.required_token import required_token
from application.utilities.required_admin import required_admin
from flask import Blueprint, request, jsonify

create_task = Blueprint('create_task', __name__)

@create_task.route('/', methods=['POST'])
@required_params(['description', 'deadline', 'status_id', 'emp_id'])
@required_token()
@required_admin()
def create_task_handler(description, deadline, status_id, emp_id, user):
  try:
    task = Task(description, deadline, status_id, emp_id)
    task.save()
    
    return jsonify({ 'message': "Task created sucessfully" }), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 500
