# pylint: disable=import-error
from application.models.User import User
from application.configs import Config
from application.utilities.required_token import required_token
from flask import Blueprint, request, jsonify

get_user = Blueprint('get_user', __name__)

@get_user.route('/', methods=['GET'])
@required_token()
def get_user_handler(user):
  try:    
    return jsonify(user), 200
  except Exception as e:
    return jsonify({
      "message": str(e)
    }), 400
