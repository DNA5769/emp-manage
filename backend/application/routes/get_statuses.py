# pylint: disable=import-error
from application.models.Status import Status
from application.utilities.required_token import required_token
from flask import Blueprint, jsonify

get_statuses = Blueprint('get_statuses', __name__)

@get_statuses.route('/')
@required_token()
def get_statuses_handler(user):
  return jsonify(Status.get_all()), 200