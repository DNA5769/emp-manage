# pylint: disable=import-error
from application.models.Task import Task
from application.utilities.required_token import required_token
from application.utilities.required_admin import required_admin
from flask import Blueprint, request, jsonify

delete_task = Blueprint('delete_task', __name__)

@delete_task.route('/', methods=['DELETE'])
@required_token()
@required_admin()
def delete_task_handler(pk, user):
  try:
    task = Task.get(pk)

    if task is None:
      return jsonify({ 'message': f"Task with id {pk} not found" }), 404

    task.delete()
    
    return jsonify({ 'message': "Task deleted sucessfully" }), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 500
