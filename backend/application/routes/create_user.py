# pylint: disable=import-error
from application.models.User import User
from application.utilities.required_params import required_params
from flask import Blueprint, request, jsonify

create_user = Blueprint('create_user', __name__)

@create_user.route('/', methods=['POST'])
@required_params(['name', 'email', 'password'])
def create_user_handler(name, email, password):
  try:
    user = User(name, email, password)
    user.save()
    
    return jsonify({ 'message': "User created sucessfully" }), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 500
