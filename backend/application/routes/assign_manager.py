# pylint: disable=import-error
from application.models.User import User
from application.utilities.required_token import required_token
from application.utilities.required_admin import required_admin
from flask import Blueprint, request, jsonify

assign_manager = Blueprint('assign_manager', __name__)

@assign_manager.route('/', methods=['POST'])
@required_token()
@required_admin()
def assign_manager_handler(pk, man, user):
  try:
    user = User.get(pk)

    if user is None:
      return jsonify({ 'message': f"User with id {pk} not found" }), 404

    user.assign(man)
    
    return jsonify({ 'message': "Manager assigned sucessfully" }), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 500
