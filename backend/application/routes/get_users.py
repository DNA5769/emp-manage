# pylint: disable=import-error
from application.models.User import User
from application.utilities.required_token import required_token
from application.utilities.required_admin import required_admin
from flask import Blueprint, jsonify

get_users = Blueprint('get_users', __name__)

@get_users.route('/')
@required_token()
@required_admin()
def home(user):
  return jsonify(User.get_all()), 200