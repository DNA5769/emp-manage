# pylint: disable=import-error
from application.models.User import User
from application.configs import Config
from application.utilities.required_basic_auth import required_basic_auth
from flask import Blueprint, request, jsonify
import jwt
import base64
from datetime import datetime, timedelta

login_user = Blueprint('login_user', __name__)

@login_user.route('/', methods=['POST'])
@required_basic_auth()
def login_user_handler(email, password):
  try:
    user = User.get_login(email, password)
    
    if user is None:
      return jsonify({ "message": "Invalid Credentials!" }), 401

    token = jwt.encode({ 'exp': datetime.utcnow() + timedelta(minutes=30) }, Config.SECRET_KEY, algorithm="HS256")
    user.login(token)
    
    return jsonify({ "token": token }), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 400
