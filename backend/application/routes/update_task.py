# pylint: disable=import-error
from application.models.Task import Task
from application.utilities.required_params import required_params
from application.utilities.required_token import required_token
from flask import Blueprint, request, jsonify

update_task = Blueprint('update_task', __name__)

@update_task.route('/', methods=['POST'])
@required_params(['description', 'deadline', 'status_id', 'emp_id'])
@required_token()
def update_task_handler(pk, description, deadline, status_id, emp_id, user):
  try:
    task = Task.get(pk)

    if task is None:
      return jsonify({ 'message': f"Task with id {pk} not found" }), 404

    task.update(description, deadline, status_id, emp_id)
    
    return jsonify({ 'message': "Task created sucessfully" }), 200
  except Exception as e:
    print(e)
    return jsonify({
      "message": str(e)
    }), 500
