from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from application.configs import Config

app = Flask(__name__)
CORS(app)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

@app.route('/')
def home():
  return jsonify({ 'message': "Hello World!" }), 200

with app.app_context():
  from application.routes.create_user import create_user
  from application.routes.get_users import get_users
  from application.routes.login_user import login_user
  from application.routes.get_user import get_user
  from application.routes.get_statuses import get_statuses
  from application.routes.create_task import create_task
  from application.routes.update_task import update_task
  from application.routes.get_user_tasks import get_user_tasks
  from application.routes.delete_task import delete_task
  from application.routes.get_tasks import get_tasks
  from application.routes.assign_manager import assign_manager

app.register_blueprint(create_user, url_prefix='/api/create_user/')
app.register_blueprint(get_users, url_prefix='/api/get_users/')
app.register_blueprint(login_user, url_prefix='/api/login_user/')
app.register_blueprint(get_user, url_prefix='/api/get_user/')
app.register_blueprint(get_statuses, url_prefix='/api/get_statuses/')
app.register_blueprint(create_task, url_prefix='/api/create_task/')
app.register_blueprint(update_task, url_prefix='/api/update_task/<int:pk>/')
app.register_blueprint(get_user_tasks, url_prefix='/api/get_user_tasks/<int:pk>/')
app.register_blueprint(delete_task, url_prefix='/api/delete_task/<int:pk>/')
app.register_blueprint(assign_manager, url_prefix='/api/assign_manager/<int:pk>/<int:man>/')
app.register_blueprint(get_tasks, url_prefix='/api/get_tasks/')
