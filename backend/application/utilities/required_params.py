from flask import request, jsonify

def required_params(*args, **kwargs):
  def wrapper(func):
    params = args[0]
    
    def inner(*args, **kwargs):
      try:
        body = request.json
      except:
        return jsonify({ "message": f"Request should contain a json body of the form {{ {', '.join(params)} }}" }), 401
      
      if body is None:
        return jsonify({ "message": f"Request should contain a json body of the form {{ {', '.join(params)} }}" }), 401

      for param in params:
        if param not in body:
          return jsonify({ "message": f"Request body of the form {{ {', '.join(params)} }}. '{param}' not found" }), 401
        kwargs.update({ param: body[param] })

      return func(*args, **kwargs)
    return inner
  return wrapper