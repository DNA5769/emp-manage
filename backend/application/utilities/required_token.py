# pylint: disable=import-error
from application.models.User import User
from application.configs import Config
from flask import request, jsonify
import jwt

def required_token(*args, **kwargs):
  def wrapper(func):
    def inner(*args, **kwargs):
      try:
        auth = request.headers['Authorization']
      except:
        return jsonify({ "message": f"Request should contain Authorization header of type Bearer Token" }), 401

      if not auth.startswith('Bearer '):
        return jsonify({ "message": f"Request should contain Authorization header of type Bearer Token" }), 401

      token = auth[7:]

      user = User.get_token(token)
    
      if user is None:
        return jsonify({ "message": "Please log in first!" }), 401

      try:
        jwt.decode(token, Config.SECRET_KEY, algorithms=["HS256"])
      except jwt.exceptions.ExpiredSignatureError:
        user.logout()
        return jsonify({ 'message': "Session has expired!" }), 401
      except jwt.exceptions.InvalidTokenError:
        return jsonify({ 'message': "Invalid Token!" }), 401
      
      kwargs.update({ 'user': user.to_json() })

      return func(*args, **kwargs)
    return inner
  return wrapper