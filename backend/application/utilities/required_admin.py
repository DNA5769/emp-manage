# pylint: disable=import-error
from flask import request, jsonify

def required_admin(*args, **kwargs):
  def wrapper(func):
    def inner(*args, **kwargs):
      user = kwargs['user']

      if not user['isAdmin']:
        return jsonify({ "message": "User should be admin" }), 401

      return func(*args, **kwargs)
    return inner
  return wrapper