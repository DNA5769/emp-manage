from flask import request, jsonify
import base64

def required_basic_auth(*args, **kwargs):
  def wrapper(func):
    def inner(*args, **kwargs):
      try:
        auth = request.headers['Authorization']
      except:
        return jsonify({ "message": f"Request should contain Authorization header of type Basic Auth" }), 401

      if not auth.startswith('Basic '):
        return jsonify({ "message": f"Request should contain Authorization header of type Basic Auth" }), 401

      try:
        (email, password) = base64.b64decode(auth[6:].encode("ascii")).decode("ascii").split(':')
      except:
        return jsonify({ "message": f"Authorization header isn't of proper Base64 encoding of 'username:password'" }), 401

      kwargs.update({ 'email': email })
      kwargs.update({ 'password': password })

      return func(*args, **kwargs)
    return inner
  return wrapper