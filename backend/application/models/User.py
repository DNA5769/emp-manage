# pylint: disable=import-error
from application import db
from application.models.Role import Role
from application.models.Task import Task
from datetime import datetime
from sqlalchemy import and_

class User(db.Model):
  __tablename__ = 'user_emp'
  id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
  name = db.Column('name', db.String(100))
  email = db.Column('email', db.String(100), unique=True)
  token = db.Column('token', db.String(500))
  role_id = db.Column(db.Integer, db.ForeignKey('role_emp.id'), default=lambda: 1)
  password = db.Column('password', db.String(100))
  manager_id = db.Column(db.Integer, db.ForeignKey('user_emp.id'))
  manager = db.relationship('User', backref="employees", remote_side=id)
  tasks = db.relationship('Task', backref="emp")
  createdAt = db.Column('createdAt', db.DateTime, default=datetime.utcnow)

  def __init__(self, name, email, password):
    self.name = name
    self.email = email
    self.password = password

  def save(self):
    db.session.add(self)
    db.session.commit()

  def login(self, token):
    self.token = token
    db.session.commit()

  def logout(self):
    self.token = None
    db.session.commit()
  
  def assign(self, man):
    if man == 0:
      self.manager_id = None
    else:
      self.manager_id = man
    db.session.commit()

  def update(self, body):
    for x in body:
      if x == 'email':
        self.email = body[x]
        self.updatedAt = datetime.utcnow()
      elif x == 'phone':
        self.phone = body[x]
        self.updatedAt = datetime.utcnow()
      elif x == 'name':
        self.name = body[x]
        self.updatedAt = datetime.utcnow()
    db.session.commit()

  def to_json(self):
    return {
      "id": self.id,
      "name": self.name,
      "email": self.email,
      "tasks": [task.to_json() for task in self.tasks if not task.isdeleted],
      "employees": [emp.to_min_json() for emp in self.employees],
      "manager": None if self.manager is None else self.manager.to_min_json(),
      "isAdmin": self.role_id == 3,
    }

  def to_min_json(self):
    return {
      "id": self.id,
      "name": self.name,
      "email": self.email,
    }
  
  def get_tasks(self):
    return [task.to_json() for task in self.tasks if not task.isdeleted]

  @classmethod
  def get(cls, id):
    return cls.query.filter(cls.id == id).first()

  @classmethod
  def get_login(cls, email, password):
    return cls.query.filter(and_(cls.email == email, cls.password == password)).first()
  
  @classmethod
  def get_token(cls, token):
    return cls.query.filter(cls.token == token).first()

  @classmethod
  def get_all(cls):
    return [u.to_json() for u in cls.query.filter(cls.role_id != 3)]
