# pylint: disable=import-error
from application import db

class Role(db.Model):
  __tablename__ = 'role_emp'
  id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
  name = db.Column('name', db.String(100))
  users = db.relationship('User')

  @classmethod
  def get(cls, id):
    role = cls.query.filter(cls.id == id).first()
    return [u.to_json() for u in role.users]
