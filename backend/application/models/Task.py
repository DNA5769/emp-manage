# pylint: disable=import-error
from application import db
from application.models.Status import Status
from datetime import datetime
from sqlalchemy import not_

class Task(db.Model):
  __tablename__ = 'task_emp'
  id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
  description = db.Column('description', db.String(300))
  deadline = db.Column('deadline', db.DateTime)
  status_id = db.Column(db.Integer, db.ForeignKey('status_emp.id'))
  status = db.relationship('Status')
  isdeleted = db.Column('isdeleted', db.Boolean, default=lambda: False)
  emp_id = db.Column(db.Integer, db.ForeignKey('user_emp.id'))
  createdAt = db.Column('createdAt', db.DateTime, default=datetime.utcnow)
  updatedAt = db.Column('updatedAt', db.DateTime)

  def __init__(self, description, deadline, status_id, emp_id):
    self.description = description
    self.deadline = deadline
    self.status_id = status_id
    self.emp_id = emp_id

  def save(self):
    db.session.add(self)
    db.session.commit()

  def delete(self):
    self.isdeleted = True
    db.session.commit()
  
  def undelete(self):
    self.isdeleted = False
    db.session.commit()

  def update(self, description, deadline, status_id, emp_id):
    self.description = description
    self.deadline = deadline
    self.status_id = status_id
    self.emp_id = emp_id
    db.session.commit()

  def to_json(self):
    return {
      "id": self.id,
      "description": self.description,
      "deadline": self.deadline,
      "status": self.status.to_json(),
      "isdeleted": self.isdeleted,
      "emp": self.emp.to_min_json(),
    }

  @classmethod
  def get(cls, id):
    return cls.query.filter(cls.id == id).first()

  @classmethod
  def get_login(cls, email, password):
    return cls.query.filter(and_(cls.email == email, cls.password == password)).first()
  
  @classmethod
  def get_all(cls):
    return [u.to_json() for u in cls.query.filter(not_(cls.isdeleted))]
