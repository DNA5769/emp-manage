# pylint: disable=import-error
from application import db

class Status(db.Model):
  __tablename__ = 'status_emp'
  id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
  name = db.Column('name', db.String(100))

  def to_json(self):
    return {
      'id': self.id,
      'name': self.name
    }

  @classmethod
  def get_all(cls):
    return [s.to_json() for s in cls.query.all()]
