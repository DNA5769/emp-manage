# Employee Management App

A web app which allows employees to view and complete their tasks along with managers being able to keep track of their employees' work. Admins, can assign/update/delete tasks to employees and even assign managers to the employees

## Technology Used
1. NextJS
2. Flask
3. MySQL: Flask-SQLAlchemy
4. Bootstrap

## Project Configuration

1. **Backend**
    - Make sure you have [Python](https://www.python.org/downloads/) installed
    - Open a terminal in the project directory
    - Move into the backend folder: `cd backend/`
    - Create a virtual environment: `python -m venv venv`
    - Activate the virtual environment: `source venv/bin/activate`
    - Install the required dependancies: `pip3 install -r requirements.txt`
    - Write your SQL database URI in the `application/config/DB.py`
    - Make the necessary migrations: 
      ```bash
      $ flask db upgrade
      $ flask db migrate
      ```
    - Run the app: `flask run`

2. **Frontend**
    - Make sure you have [NodeJS](https://nodejs.org/en/) installed
    - Open another terminal in the project directory
    - Move into the front folder: `cd frontend/`
    - Install the required dependancies: `npm i`
    - Run the app: `npm run dev`
    