import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';
import AdminHome from '../components/AdminHome';
import EmpHome from '../components/EmpHome';

export default function Home() {
  const [user, setUser] = useState({});
  const router = useRouter();

  useEffect(() => {
    if (typeof(window) !== 'undefined')
    {
      if (localStorage.getItem('token') === null)
      {
        toast.error('Please login first!', {
          position: "bottom-left",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        router.push('/login');
      }
      else
      {
        let token = localStorage.getItem('token');
        axios.get(`${process.env.API_URL}get_user/`, { headers: {
          'Authorization': `Bearer ${token}` 
        }}).then(res => {
          let user_data = res.data;
          user_data.token = token;
          setUser(user_data);
        })
        .catch(err => {
          toast.error(err.response && err.response.data.message === "Session has expired!" ? err.response.data.message : err.response && err.response.data.message === "Invalid Token!" ? err.response.data.message : 'Something went wrong!', {
            position: "bottom-left",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          localStorage.removeItem('token');
          router.push('/login');
        });
      }
    }
  }, []);

  return (
    <div>
      {user.isAdmin ? <AdminHome user={user} /> : <EmpHome user={user} />}
    </div>
  );
}
