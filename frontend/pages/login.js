import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';

import LogInForm from '../components/LogInForm';
import Navbar from "../components/Navbar";
import SignUpForm from "../components/SignUpForm";

export default function Login() {
  const [isLogin, setIsLogin] = useState(true);

  const router = useRouter();

  useEffect(() => {
    if (typeof(window) !== 'undefined' && localStorage.getItem('token') !== null)
      router.push('/');
  }, []);

  return (
    <>
      <Navbar page="/login" setIsLogin={setIsLogin} />
      {isLogin ? <LogInForm /> : <SignUpForm setIsLogin={setIsLogin} />}
    </>
  );
}
