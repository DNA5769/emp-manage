import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';

const TaskCard = ({ task, i, user, setTasks, tasks, isAdmin, isManage }) => {
  const formatDate = date => {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) 
      month = '0' + month;
    if (day.length < 2) 
      day = '0' + day;

    return [year, month, day].join('-');
  }

  const [update, setUpdate] = useState(false);
  const [desc, setDesc] = useState({ value: task.description, first: true });
  const [deadline, setDeadline] = useState({ value: formatDate(Date.parse(task.deadline.split(' ').splice(1, 3).join(' '))), first: true });
  const [status, setStatus] = useState({ value: task.status.id, first: true });
  const [statuses, setStatuses] = useState([]);
  const [emp, setEmp] = useState({ value: task.emp.id, first: true });
  const [emps, setEmps] = useState([]);

  const router = useRouter();

  useEffect(() => {
    axios.get(`${process.env.API_URL}get_statuses/`, { headers: {
      'Authorization': `Bearer ${user.token}` 
    }}).then(res => {
      setStatuses(res.data);
    })
    .catch(err => {
      toast.error(err.response && err.response.data.message === "Session has expired!" ? err.response.data.message : err.response && err.response.data.message === "Invalid Token!" ? err.response.data.message : 'Something went wrong!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      localStorage.removeItem('token');
      router.push('/login');
    });

    if (isAdmin)
    {
      axios.get(`${process.env.API_URL}get_users/`, { headers: {
        'Authorization': `Bearer ${user.token}` 
      }}).then(res => {
        setEmps(res.data);
      })
      .catch(err => {
        toast.error(err.response && err.response.data.message === "Session has expired!" ? err.response.data.message : err.response && err.response.data.message === "Invalid Token!" ? err.response.data.message : 'Something went wrong!', {
          position: "bottom-left",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        localStorage.removeItem('token');
        router.push('/login');
      });
    }
  }, []);

  const handleSubmit = () => {
    axios.post(`${process.env.API_URL}update_task/${task.id}/`, {
      description: desc.value,
      deadline: deadline.value,
      status_id: parseInt(status.value),
      emp_id: parseInt(emp.value)
    }, { headers: {
      'Authorization': `Bearer ${user.token}` 
    }}).then(res => {
      if (isAdmin)
        setTasks(tasks.map(t => {
          let ded = `${new Date(deadline.value)}`.split(' ')
          let swap = ded[1];
          ded[1] = ded[2];
          ded[2] = swap;
          console.log(ded)
          if (t.id !== task.id)
            return t;
          else
            return {
              id: t.id,
              description: desc.value,
              deadline: `${ded.join(' ')}`,
              status: { id: parseInt(status.value), name: statuses.filter(s => s.id === parseInt(status.value))[0].name },
              emp: { id: parseInt(emp.value), name: emps.filter(e => e.id === parseInt(emp.value))[0].name },
              isdeleted: t.isdeleted
            };
        }));
      else
      setTasks(tasks.map(t => {
        if (t.id !== task.id)
          return t;
        else
          return {
            id: t.id,
            description: desc.value,
            deadline: t.deadline,
            status: { id: parseInt(status.value), name: statuses.filter(s => s.id === parseInt(status.value))[0].name },
            emp: { id: t.emp.id, name: t.emp.name },
            isdeleted: t.isdeleted
          };
      }));
      toast.success('Successfully updated task!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    })
    .catch(err => toast.error('Something went wrong!', {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }));

    setUpdate(false);
  };

  const handleDelete = () => {
    axios.delete(`${process.env.API_URL}delete_task/${task.id}/`, { headers: {
      'Authorization': `Bearer ${user.token}` 
    }}).then(res => {
      console.log(task);
      setTasks(tasks.filter(t => t.id !== task.id));
      toast.success('Successfully deleted task!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    })
    .catch(err => toast.error('Something went wrong!', {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }));
  };
  
  return (
    <tr>
      <th scope="row">{i+1}</th>
      {!update ?
        <>
          <td>{task.description}</td>
          <td>{task.emp.name}</td>
          <td>{task.status.name}</td>
          <td>{task.deadline.split(' ').splice(1, 3).join(' ')}</td>
          { !isManage ? <td><button type="button" className={`btn mx-auto d-block btn-primary`} onClick={() => setUpdate(!update)}>Update</button></td> : null}
          { isAdmin ? <td><button type="button" className={`btn mx-auto d-block btn-danger`} onClick={handleDelete}>Delete</button></td> : null}
        </> : isAdmin ?
        <>
          <td>
            <input type="text" className={`form-control${desc.first ? desc.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setDesc({ value: e.target.value, first: true })} value={desc.value} />
            <div className="invalid-feedback">
              Please enter a description
            </div>
          </td>
          <td>
            <select className={`form-select form-control${emp.first ? emp.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setEmp({ value: e.target.value, first: true })} value={emp.value}>
            <option value="">...</option>
              {emps.map(emp => <option key={emp.id} value={emp.id}>{emp.name}</option>)}
            </select>
            <div className="invalid-feedback">
              Please select a employee
            </div>
          </td>
          <td>
            <select className={`form-select form-control${status.first ? status.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setStatus({ value: e.target.value, first: true })} value={status.value}>
            <option value="">...</option>
              {statuses.map(status => <option key={status.id} value={status.id}>{status.name}</option>)}
            </select>
            <div className="invalid-feedback">
              Please select a status
            </div>
          </td>
          <td>
            <input type="date" className={`form-control${deadline.first ? deadline.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setDeadline({ value: e.target.value, first: true })} value={deadline.value} />
            <div className="invalid-feedback">
              Please set a deadline
            </div>
          </td>
          <td><button type="button" className={`btn mx-auto d-block btn-success${desc.value!=='' && deadline.value!=='' && status.value!=='' && emp.value!=='' ? `` : ` disabled`}`} onClick={handleSubmit}>Confirm</button></td>
          <td><button type="button" className={`btn mx-auto d-block btn-danger`} onClick={handleDelete}>Delete</button></td>
        </> : 
        <>
          <td>{task.description}</td>
          <td>{task.emp.name}</td>
          <td>
            <select className={`form-select form-control${status.first ? status.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setStatus({ value: e.target.value, first: true })} value={status.value}>
            <option value="">...</option>
              {statuses.map(status => <option key={status.id} value={status.id}>{status.name}</option>)}
            </select>
            <div className="invalid-feedback">
              Please select a status
            </div>
          </td>
          <td>{task.deadline.split(' ').splice(1, 3).join(' ')}</td>
          <td><button type="button" className={`btn mx-auto d-block btn-success${desc.value!=='' && deadline.value!=='' && status.value!=='' && emp.value!=='' ? `` : ` disabled`}`} onClick={handleSubmit}>Confirm</button></td>
        </>
      }
    </tr>
  );
}

export default TaskCard;
