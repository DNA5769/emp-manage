import { useRouter } from 'next/router';

const Navbar = ({ page, setIsLogin, isAdmin, section, setSection }) => {
  const router = useRouter();

  const logOut = () => {
    if (typeof(window) !== 'undefined')
    {
      localStorage.removeItem('token');
      router.push('/login');
    }
  };

  return (
    <nav className="navbar navbar-expand-md navbar-dark bg-dark mb-4">
    <div className="container-fluid">
      <a className="navbar-brand" href="#">Employee Management</a>
      <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarCollapse">
        <ul className="navbar-nav me-auto mb-2 mb-md-0">
          {page === "/" ? isAdmin ? 
            <>
              <li className="nav-item">
                <a className={`nav-link${section === "create" ? ` active` : ``}`} aria-current="page" href="#" onClick={e => { e.preventDefault(); setSection('create') }}>Create Task</a>
              </li>
              <li className="nav-item">
                <a className={`nav-link${section === "view" ? ` active` : ``}`} aria-current="page" href="#" onClick={e => { e.preventDefault(); setSection('view') }}>View Tasks</a>
              </li>
              <li className="nav-item">
                <a className={`nav-link${section === "assign" ? ` active` : ``}`} aria-current="page" href="#" onClick={e => { e.preventDefault(); setSection('assign') }}>Assign Manager</a>
              </li>
            </> :
            <>
              <li className="nav-item">
                <a className={`nav-link${section === "view" ? ` active` : ``}`} aria-current="page" href="#" onClick={e => { e.preventDefault(); setSection('view') }}>View Tasks</a>
              </li>
              <li className="nav-item">
                <a className={`nav-link${section === "manage" ? ` active` : ``}`} aria-current="page" href="#" onClick={e => { e.preventDefault(); setSection('manage') }}>Manage</a>
              </li>
            </> : null
          }
        </ul>
        <form className="d-flex">
          {page === "/login" ? 
            <>
              <button className="btn btn-outline-primary me-2" onClick={e => { e.preventDefault(); setIsLogin(false) }}>Sign Up</button>
              <button className="btn btn-outline-success" onClick={e => { e.preventDefault(); setIsLogin(true) }}>Log In</button>
            </> :
            <button className="btn btn-outline-warning me-2" onClick={logOut}>Log Out</button>
          }
        </form>
      </div>
    </div>
  </nav>
  );
}

export default Navbar;
