import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';

const SignUpForm = ({ user }) => {
  const [desc, setDesc] = useState({ value: '', first: false });
  const [deadline, setDeadline] = useState({ value: '', first: false });
  const [status, setStatus] = useState({ value: '', first: false });
  const [statuses, setStatuses] = useState([]);
  const [emp, setEmp] = useState({ value: '', first: false });
  const [emps, setEmps] = useState([]);

  const router = useRouter();

  useEffect(() => {
    axios.get(`${process.env.API_URL}get_statuses/`, { headers: {
      'Authorization': `Bearer ${user.token}` 
    }}).then(res => {
      setStatuses(res.data);
    })
    .catch(err => {
      toast.error(err.response && err.response.data.message === "Session has expired!" ? err.response.data.message : err.response && err.response.data.message === "Invalid Token!" ? err.response.data.message : 'Something went wrong!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      localStorage.removeItem('token');
      router.push('/login');
    });

    axios.get(`${process.env.API_URL}get_users/`, { headers: {
      'Authorization': `Bearer ${user.token}` 
    }}).then(res => {
      setEmps(res.data);
    })
    .catch(err => {
      toast.error(err.response && err.response.data.message === "Session has expired!" ? err.response.data.message : err.response && err.response.data.message === "Invalid Token!" ? err.response.data.message : 'Something went wrong!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      localStorage.removeItem('token');
      router.push('/login');
    });
  }, [])
  
  const handleSubmit = e => {
    e.preventDefault();

    axios.post(`${process.env.API_URL}create_task/`, {
      description: desc.value,
      deadline: deadline.value,
      status_id: parseInt(status.value),
      emp_id: parseInt(emp.value)
    }, { headers: {
      'Authorization': `Bearer ${user.token}` 
    }}).then(res => {
      toast.success('Successfully created task!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    })
    .catch(err => toast.error('Something went wrong!', {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }));
  };

  return (
    <div className="container">
      <div className="row">
        <div className="d-none d-md-block col-md-3 col-lg-4"></div>
        <div className="col-12 col-md-6 col-lg-3">
          <h2>Create Task</h2>
          <div className="mb-3">
            <div>
              <label className="form-label">Description</label>
              <input type="text" className={`form-control${desc.first ? desc.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setDesc({ value: e.target.value, first: true })} value={desc.value} />
              <div className="invalid-feedback">
                Please enter a description
              </div>
            </div>
            <div>
              <label className="form-label">Deadline</label>
              <input type="date" className={`form-control${deadline.first ? deadline.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setDeadline({ value: e.target.value, first: true })} value={deadline.value} />
              <div className="invalid-feedback">
                Please set a deadline
              </div>
            </div>
            <div>
              <label className="form-label">Status</label>
              <select className={`form-select form-control${status.first ? status.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setStatus({ value: e.target.value, first: true })} value={status.value}>
                <option value="">...</option>
                {statuses.map(status => <option key={status.id} value={status.id}>{status.name}</option>)}
              </select>
              <div className="invalid-feedback">
                Please select a status
              </div>
            </div>
            <div>
              <label className="form-label">Employee</label>
              <select className={`form-select form-control${emp.first ? emp.value !== '' ? ` is-valid` : ` is-invalid` : ``}`} onChange={e => setEmp({ value: e.target.value, first: true })} value={emp.value}>
                <option value="">...</option>
                {emps.map(emp => <option key={emp.id} value={emp.id}>{emp.name}</option>)}
              </select>
              <div className="invalid-feedback">
                Please select a employee
              </div>
            </div>
          </div>
          <div className="w-100">
            <button type="button" className={`btn mx-auto d-block btn-outline-primary btn-lg${desc.value!=='' && deadline.value!=='' && status.value!=='' && emp.value!=='' ? `` : ` disabled`}`} onClick={handleSubmit}>Submit</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignUpForm;
