import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';

import TaskCard from './TaskCard';

const TaskList = ({ user, isAdmin, isManage, emp }) => {
  const [tasks, setTasks] = useState([]);

  const router = useRouter();

  console.log(user)

  useEffect(() => {
    if (isAdmin)
    {
      axios.get(`${process.env.API_URL}get_tasks/`, { headers: {
        'Authorization': `Bearer ${user.token}` 
      }}).then(res => {
        console.log(res.data);
        setTasks(res.data);
      })
      .catch(err => {
        toast.error(err.response && err.response.data.message === "Session has expired!" ? err.response.data.message : err.response && err.response.data.message === "Invalid Token!" ? err.response.data.message : 'Something went wrong!', {
          position: "bottom-left",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        localStorage.removeItem('token');
        router.push('/login');
      });
    }
    else if (isManage)
    {
      if (emp === '')
        setTasks([])
      else
        axios.get(`${process.env.API_URL}get_user_tasks/${parseInt(emp)}/`, { headers: {
          'Authorization': `Bearer ${user.token}` 
        }}).then(res => {
          setTasks(res.data.tasks);
        })
        .catch(err => {
          toast.error(err.response && err.response.data.message === "Session has expired!" ? err.response.data.message : err.response && err.response.data.message === "Invalid Token!" ? err.response.data.message : 'Something went wrong!', {
            position: "bottom-left",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          localStorage.removeItem('token');
          router.push('/login');
        });
    }
    else
    {
      setTasks(user.hasOwnProperty('tasks') ? user.tasks : []);
    }
  }, isAdmin ? [] : isManage ? [emp] : [user]);

  return (
    <table className="table table-striped table-dark">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Description</th>
          <th scope="col">Employee</th>
          <th scope="col">Status</th>
          <th scope="col">Deadline</th>
          {!isManage ? <th scope="col">Update</th> : null}
          {isAdmin ? <th scope="col">Delete</th> : null}
        </tr>
      </thead>
      <tbody>
        {tasks.length === 0 ? <tr><td></td>{isManage ? null : <td></td>}<td>No</td><td>Tasks</td><td>Created</td><td></td>{isAdmin ? <td></td> : null}</tr> : tasks.map((task, i) => <TaskCard key={task.id} i={i} task={task} tasks={tasks} isAdmin={isAdmin} isManage={isManage} setTasks={setTasks} user={user}/>)}
      </tbody>
    </table>
  );
}

export default TaskList;
