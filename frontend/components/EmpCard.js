import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';

const EmpCard = ({ emp, i, user, emps, setEmps }) => {
  const [update, setUpdate] = useState(false);
  const [man, setMan] = useState({ value: emp.manager !== null ? emp.manager.id : '0', first: true });

  const router = useRouter();

  const handleSubmit = () => {
    axios.post(`${process.env.API_URL}assign_manager/${emp.id}/${man.value}/`, {}, { headers: {
      'Authorization': `Bearer ${user.token}` 
    }}).then(res => {
      setEmps(emps.map(e => {
        if (e.id !== emp.id)
          return e;
        else
          return {
            id: e.id,
            name: e.name,
            manager: man.value === '0' ? null : { id: parseInt(man.value), name: emps.filter(em => em.id === parseInt(man.value))[0].name }
          }
      }))
      toast.success('Successfully assigned manager!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    })
    .catch(err => toast.error('Something went wrong!', {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }));

    setUpdate(false);
  };
  
  return (
    <tr>
      <th scope="row">{i+1}</th>
      <td>{emp.name}</td>
      {!update ?
        <>
          <td>{emp.manager !== null ? emp.manager.name : `N/A`}</td>
          <td><button type="button" className={`btn mx-auto d-block btn-primary`} onClick={() => setUpdate(!update)}>Update</button></td>
        </> :
        <>
          <td>
            <select className={`form-select form-control`} onChange={e => setMan({ value: e.target.value, first: true })} value={man.value}>
              <option value="0">N/A</option>
              {emps.filter(e => e.id !== emp.id).map(e => <option key={e.id} value={e.id}>{e.name}</option>)}
            </select>
          </td>
          <td><button type="button" className={`btn mx-auto d-block btn-success`} onClick={handleSubmit}>Confirm</button></td>
        </>
      }
    </tr>
  );
}

export default EmpCard;
