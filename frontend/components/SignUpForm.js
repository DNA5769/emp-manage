import { useState, useContext } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';

const SignUpForm = ({ setIsLogin }) => {
  const [email, setEmail] = useState({ value: '', check: false, message: 'Please enter an email', first: false });
  const [name, setName] = useState({ value: '', check: false, first: false });
  const [password, setPassword] = useState({ value: '', check: false, message: 'Please enter an password', first: false });

  const handleEmailChange = e => {
    const { value } = e.target;

    if (value === '')
      setEmail({ value: value, check: false, first: true, message: 'Please enter an email' })
    else if (value.indexOf('@') === -1 || !value.endsWith('.com'))
      setEmail({ value: value, check: false, first: true, message: 'Invalid email' })
    else
      setEmail({ value: value, check: true, first: true, message: 'Invalid email' })
  };

  const handleNameChange = e => {
    const { value } = e.target;

    if (value === '')
      setName({ value: value, check: false, first: true })
    else
      setName({ value: value, check: true, first: true })
  };

  const handlePasswordChange = e => {
    const { value } = e.target;

    if (value === '')
      setPassword({ value: value, check: false, message: 'Please enter an password', first: true })
    else if (value.length < 6)
      setPassword({ value: value, check: false, message: 'Password too weak', first: true })
    else
      setPassword({ value: value, check: true, message: 'Please enter an password', first: true })
  };

  const handleSubmit = e => {
    e.preventDefault();

    axios.post(`${process.env.API_URL}create_user/`, {
      email: email.value,
      name: name.value,
      password: password.value,
    }).then(res => {
      toast.success('Successfully created account!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      setIsLogin(true);
    })
    .catch(err => toast.error('Something went wrong!', {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }));
  };

  return (
    <div className="container">
      <div className="row">
        <div className="d-none d-md-block col-md-3 col-lg-4"></div>
        <div className="col-12 col-md-6 col-lg-3">
          <h2>Sign Up</h2>
          <div className="mb-3">
            <div>
              <label className="form-label">Email</label>
              <input type="email" className={`form-control${email.first ? email.check ? ` is-valid` : ` is-invalid` : ``}`} onChange={handleEmailChange} value={email.value} />
              <div className="invalid-feedback">
                {email.message}
              </div>
            </div>
            <div>
              <label className="form-label">Name</label>
              <input type="text" className={`form-control${name.first ? name.check ? ` is-valid` : ` is-invalid` : ``}`} onChange={handleNameChange} value={name.value}/>
              <div className="invalid-feedback">
                Please enter a name
              </div>
            </div>
            <div>
              <label className="form-label">Password</label>
              <input type="password" className={`form-control${password.first ? password.check ? ` is-valid` : ` is-invalid` : ``}`} onChange={handlePasswordChange} value={password.value} />
              <div className="invalid-feedback">
                {password.message}
              </div>
            </div>
            
          </div>
          <div className="w-100">
            <button type="button" className={`btn mx-auto d-block btn-outline-primary btn-lg${email.check && name.check && password.check ? `` : ` disabled`}`} onClick={handleSubmit}>Submit</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignUpForm;
