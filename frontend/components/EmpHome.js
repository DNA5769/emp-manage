import { useState } from 'react';
import Navbar from './Navbar';
import TaskList from './TaskList';
import ManageList from './ManageList';

const EmpHome = ({ user }) => {
  const [section, setSection] = useState("view");

  return (
    <div>
      <Navbar page="/" isAdmin={false} section={section} setSection={setSection} />
      {section === "view" ? <TaskList user={user} isAdmin={false} isManage={false} /> :
       section === "manage" ? <ManageList user={user} isAdmin={false} isManage={true}/> : <></>}
    </div>
  );
}

export default EmpHome;
