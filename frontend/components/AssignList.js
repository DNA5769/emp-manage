import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';

import EmpCard from './EmpCard';

const AssignList = ({ user }) => {
  const [emps, setEmps] = useState([]);

  const router = useRouter();

  useEffect(() => {
    axios.get(`${process.env.API_URL}get_users/`, { headers: {
      'Authorization': `Bearer ${user.token}` 
    }}).then(res => {
      console.log(res.data)
      setEmps(res.data);
    })
    .catch(err => {
      toast.error(err.response && err.response.data.message === "Session has expired!" ? err.response.data.message : err.response && err.response.data.message === "Invalid Token!" ? err.response.data.message : 'Something went wrong!', {
        position: "bottom-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      localStorage.removeItem('token');
      router.push('/login');
    });
  }, []);

  return (
    <table className="table table-striped table-dark">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Employee</th>
          <th scope="col">Manager</th>
          <th scope="col">Update</th>
        </tr>
      </thead>
      <tbody>
        {emps.map((emp, i) => <EmpCard key={emp.id} i={i} emp={emp} emps={emps} user={user} setEmps={setEmps}/>)}
      </tbody>
    </table>
  );
}

export default AssignList;
