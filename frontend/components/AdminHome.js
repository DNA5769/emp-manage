import { useState } from 'react';
import Navbar from './Navbar';
import CreateTaskForm from './CreateTaskForm';
import TaskList from './TaskList';
import AssignList from './AssignList';

const AdminHome = ({ user }) => {
  const [section, setSection] = useState("create");

  return (
    <div>
      <Navbar page="/" isAdmin={true} section={section} setSection={setSection} />
      {section === "create" ? <CreateTaskForm user={user} /> :
       section === "view" ? <TaskList user={user} isAdmin={true} isManage={true} /> :
       section === "assign" ? <AssignList user={user} /> : <></>}
    </div>
  );
}

export default AdminHome;
