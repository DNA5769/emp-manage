import { useState, useEffect } from 'react';

import TaskList from './TaskList';

const ManageList = ({ user, isAdmin, isManage }) => {
  const [emps, setEmps] = useState([]);
  const [emp, setEmp] = useState("");

  useEffect(() => {
    setEmps(user.hasOwnProperty('employees') ? user.employees : []);
  }, isAdmin ? [] : [user]);

  return (
    <div>
      <div className="mx-2 my-3">
        <select className={`form-select form-control`} onChange={e => setEmp(e.target.value)} value={emp.value}>
          <option value="">Choose an Employee</option>
          {emps.map(emp => <option key={emp.id} value={emp.id}>{emp.name}</option>)}
        </select>
      </div>
      {emp !== "" ? <TaskList user={user} isAdmin={false} isManage={true} emp={emp} /> : null}
    </div>
  );
}

export default ManageList;
